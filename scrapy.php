<?php
define('URL_PROXY_FRESH_RU', 'http://proxy-fresh.ru');

#Database settings
define('DB_HOST', 'localhost');
define('DB_NAME', 'proxy_list');
define('DB_TABLE_NAME', 'proxy');
define('DB_USER', 'root');
define('DB_PASSWORD', '24zda#1611');


class DBConnector
{
    private $connectionDb;

    public function __construct()
    {
        if (class_exists("PDO") && in_array("mysql", PDO::getAvailableDrivers())) {
            $this->connectionDb = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASSWORD, array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ));
        } else {
            $this->connectionDb = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
            mysqli_select_db($this->connectionDb, DB_NAME);
            mysqli_set_charset($this->connectionDb, "utf8");
        }
    }

    public function executeQuery($query)
    {
        echo $query . "\n";
        return class_exists("PDO") ? $this->connectionDb->query($query) : mysqli_query($this->connectionDb, $query);
    }

    public function closeConnection()
    {
        $this->connectionDb->close();
    }

    public function getDbConnector()
    {
        return $this;
    }

}


class Scrapy
{
    private $dbConnector;

    /**
     * Scrapy constructor.
     */
    public function __construct(DBConnector $dbConnector)
    {
        $this->dbConnector = $dbConnector;
    }

    /**
     * парсинг сайта URL_PROXY_FRESH_RU
     */
    public function scraping_site_free_proxy_list()
    {
        $answer = $this->sendRequest(URL_PROXY_FRESH_RU . "/proxy/type/https/");

        if (preg_match("/\'dynamicBlocks':{'(\w+)':{'hash':'(\w+)'}}/", $answer, $matcher_bitrix_cache_param)) {//Данные для формирования BX заголовков
            $dynamicBlockName = $matcher_bitrix_cache_param[1][0];
            $dynamicHash = $matcher_bitrix_cache_param[2][0];
            $arrayHeader = array(
                'Accept: */*',
                'Accept-Encoding: gzip, deflate',
                'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'BX-ACTION-TYPE: get_dynamic',
                'Connection: keep-alive',
                'Host: proxy-fresh.ru',
                'Referer: http://proxy-fresh.ru/',
                'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
                'BX-CACHE-BLOCKS: {"' . $dynamicBlockName . '":{"hash":' . $dynamicHash . '}}',//BX заголовоки для получения COOKIE
                'BX-CACHE-MODE:HTMLCACHE');

            //Запрос для получения Cookeis
            $this->sendRequest(URL_PROXY_FRESH_RU, $array_header = $arrayHeader, $send_cookie = true, $post_fields = null);

            if (preg_match_all("/<a class=\"ui header\" href=\"(.+?)\">.+?<span>(\d{2}\.\d{2}\.\d{4})\. обновлено (\d{1,2}:\d{1,2})<\/span>/", $answer, $matches)) { //Дата обновления
                $urlPageDownloadFile = null;
                $lastCheckTime = 0;
                for ($i = 0; $i < count($matches); $i++) {
                    if (strtotime($matches[2][$i] . " " . $matches[3][$i]) > $lastCheckTime) {
                        $urlPageDownloadFile = $matches[1][$i];
                        $lastCheckTime = strtotime($matches[2][$i] . " " . $matches[3][$i]);
                    }
                }

                $answer_page_download = $this->sendRequest(URL_PROXY_FRESH_RU . $urlPageDownloadFile, $array_header = array(), $send_cookie = true, $post_fields = null);
                if (
                    preg_match("/<button class=\"ui button\" rel=\"download\" data-hash=\"(\w+)\".+?>/", $answer_page_download, $arrayHashDownload)
                    &&
                    preg_match("/'bitrix_sessid':'(\w+)'/", $answer_page_download, $array_bitrix_ses_id)
                ) {
                    $hash_download = $arrayHashDownload[1];
                    $bitrix_session_id = $array_bitrix_ses_id[1];
                    //Запросы для проверки bitrix_sessionid and hash download
                    $this->sendRequest(URL_PROXY_FRESH_RU . "/ajax/index.php", array(), true, "action=checksessid&sessid=" . $bitrix_session_id . "&hash=" . $hash_download);
                    echo $this->sendRequest(URL_PROXY_FRESH_RU . "/ajax/index.php", array(), true, "action=getlink&sessid=" . $bitrix_session_id);
                    $proxyList = $this->sendRequest(URL_PROXY_FRESH_RU . "/download/", $array_header = array(), $send_cookie = true, $post_fields = null);
                    $arProxy = explode("\r\n", $proxyList);
                    foreach ($arProxy as $line) {
                        $explodeLine = explode(":", $line);
                        if ($explodeLine[0] && $explodeLine[1]) {
                            $this->dbConnector->executeQuery("REPLACE INTO " . DB_TABLE_NAME . "(proxy_address, proxy_port) VALUES ('" . $explodeLine[0] . "', " . $port = $explodeLine[1] . ")");
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $url $url request
     * @param array $array_header Переопределение заголовков
     * @param bool $send_cookie Отправлять cookies
     * @param null $post_fields Данные для POST запроса
     * @return mixed Ответ от сервера
     */
    function sendRequest($url, $array_header = array(), $send_cookie = false, $post_fields = null)
    {
        static $nextAccessTime;
        $timeRequest = microtime(true);
        if ($nextAccessTime > $timeRequest) {
            usleep(intval(($nextAccessTime - $timeRequest) * 1000000));
        }
        $nextAccessTime = $timeRequest + intval(rand(5, 10));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0');
        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '\cookie.txt');

        if (count($array_header) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $array_header);
        }
        if ($send_cookie) {
            curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '\cookie.txt');
        }
        if (!empty($post_fields)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
        }

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
}

$dbConnector = new DBConnector();
$scrapy = new Scrapy($dbConnector);

$scrapy->scraping_site_free_proxy_list();